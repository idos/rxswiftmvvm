//
//  ServiceAPI.swift
//  RxSwiftMvvm
//
//  Created by R+IOS on 15/06/22.
//

import Foundation

class ServiceAPI {
    func requestPersons(completion: @escaping (_ result: Result<[Person]?, Error>) -> Void) {
        request { result in
            switch result {
            case .success(let data):
                guard let persons = try? JSONDecoder().decode([Person].self, from: data) else {
                    completion(.failure("Data emptu" as! Error))
                    return
                }
                
                DispatchQueue.main.async {
                    completion(.success(persons))
                }
            case .failure(let error):
                print(error)
                completion(.failure(error))
            }
        }
    }
    
    func addPerson(params: [String: Any], completion: @escaping (_ result: Result<Bool, Error>) -> Void) {
        requestWithHeader(params: params) { result in
            switch result {
            case .success(_):
                completion(.success(true))
                
            case .failure(let error):
                print(error)
                completion(.failure(error))
                
            }
        }
    }
    
    private func request(method: Methods = .get, completion: @escaping (_ result: Result<Data, Error>) -> Void) {
        let session = URLSession.shared
        let url = URL(string: "http://friendservice.herokuapp.com/listFriends")!
        let task = session.dataTask(with: url, completionHandler: { data, response, error in
            // Check the response
            print(response)
            
            // Check if an error occured
            if error != nil {
                // HERE you can manage the error
                print(error!)
                completion(.failure(error!))
                return
            }
            guard let data = data else {
//                completion(nil)
                return
            }
            completion(.success(data))
            
        })
        task.resume()
    }
    
    private func requestWithHeader(method: Methods = .post, params: [String: Any], completion: @escaping (_ result: Result<Data, Error>) -> Void) {

        var request = URLRequest(url: URL(string: "https://friendservice.herokuapp.com/addFriend")!)
        request.httpMethod = method.rawValue
        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            print(response!)
            // Check if an error occured
            if error != nil {
                // HERE you can manage the error
                print(error!)
                completion(.failure(error!))
                return
            }
            guard let data = data else {
//                completion(nil)
                return
            }
            completion(.success(data))
        })

        task.resume()
    }
}

enum Methods: String {
    case post = "POST"
    case get = "GET"
    case delete = "DELETE"
}
