//
//  Person.swift
//  RxSwiftMvvm
//
//  Created by R+IOS on 15/06/22.
//

import Foundation

struct Person: Codable {
    let firstname: String
    let lastname: String
    let phonenumber: String
    let id: Int
}
