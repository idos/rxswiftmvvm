//
//  PersonsViewController.swift
//  RxSwiftMvvm
//
//  Created by R+IOS on 15/06/22.
//

import UIKit
import RxSwift
import RxCocoa

class PersonsViewController: UIViewController {
    
    var viewModel = PersonViewModel()
    
    let tableView: UITableView = {
        let table = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        table.register(PersonTableViewCell.self, forCellReuseIdentifier: "cell")
        table.rowHeight = 470
        table.layer.backgroundColor = UIColor.white.cgColor
        table.tableFooterView = UIView(frame: .zero)
        return table
    }(
    )
    
    private let disposeBag = DisposeBag()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .purple
        self.navigationItem.title = "Person Page"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(
            title: "Add",
            style: .plain,
            target: self,
            action: #selector(didTapView(_:))
        )
        
        tableView.rx
            .setDelegate(self)
            .disposed(by: disposeBag)

        setupConstraints()
        bindTableView()
        viewModel.getPerson()
        // Do any additional setup after loading the view.
    }
    
    fileprivate func setupConstraints() {
       view.addSubview(tableView)
       tableView.topAnchor.constraint(
        equalTo: view.safeAreaLayoutGuide.topAnchor,
        constant: 0
       ).isActive = true
        
       tableView.bottomAnchor.constraint(
        equalTo: view.bottomAnchor,
        constant: 0
       ).isActive = true
        
       tableView.leadingAnchor.constraint(
        equalTo: view.leadingAnchor,
        constant: 0
       ).isActive = true
        
       tableView.trailingAnchor.constraint(
        equalTo: view.trailingAnchor,
        constant: 0
       ).isActive = true
    }
    
    private func bindTableView() {
            
        viewModel.persons.asObservable().bind(
            to: tableView.rx.items(
                cellIdentifier: "cell",
                cellType: PersonTableViewCell.self)
        ) { (row,item,cell) in
            cell.usernameLabel.text = "\(item.firstname)" + " " + "\(item.lastname)"
            cell.phoneNumberLabel.text = item.phonenumber
            cell.profileImageView.image = UIImage(named: "profile")
        }
        .disposed(by: disposeBag)
        
        //            tableView.rx.modelSelected(Product.self).subscribe(onNext: { item in
        //                print("SelectedItem: \(item.name)")
        //            }).disposed(by: bag)
        //
    }
    
    @objc func didTapView(_ sender: Any) {
        print("did tap view", sender)
        let vc = AddPersonViewController()
        vc.callback = { [weak self] in
            self?.viewModel.getPerson()
        }
        self.navigationController?.pushViewController(vc, animated: false)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PersonsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}
