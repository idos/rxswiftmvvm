//
//  AddPersonViewController.swift
//  RxSwiftMvvm
//
//  Created by R+IOS on 15/06/22.
//

import UIKit
import RxSwift
import RxCocoa

class AddPersonViewController: UIViewController {
    
    let topView: UIView = {
        let view = UIView()
        view.backgroundColor = .orange
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    let firstNameTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.placeholder = "First Name"
        tf.borderStyle = .roundedRect
        tf.backgroundColor = UIColor(white: 0, alpha: 0.1)
        return tf
        
    }()
    
    let lastNameTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.placeholder = "Last Name"
        tf.borderStyle = .roundedRect
        tf.backgroundColor = UIColor(white: 0, alpha: 0.1)
        return tf
        
    }()
    
    let phoneNumberTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.placeholder = "+67382732"
        tf.borderStyle = .roundedRect
        tf.backgroundColor = UIColor(white: 0, alpha: 0.1)
        return tf
        
    }()
    
    let saveButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Save", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 3
        button.backgroundColor = .systemOrange
        return button
    }()
    
    var viewModel = AddPersonViewModel()
    let disposeBag = DisposeBag()
    var callback: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupTopView()
        setupTextFields()
        binding()
    }
    
    func binding() {
        firstNameTextField.rx
            .text
            .bind(to: viewModel.firsNmae)
            .disposed(by: self.disposeBag)
        
        lastNameTextField.rx
            .text
            .bind(to: viewModel.lastName)
            .disposed(by: self.disposeBag)
        
        phoneNumberTextField.rx
            .text
            .bind(to: viewModel.phoneNumber)
            .disposed(by: self.disposeBag)
        
        viewModel.isValidForm
            .bind(to: saveButton.rx.isEnabled)
            .disposed(by: self.disposeBag)
        
        saveButton.rx.tap
            .bind{
                self.viewModel.addPerson { result in
                    if result {
                        DispatchQueue.main.async {
                           // UI work here
                            self.callback?()
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
            }
            .disposed(by: self.disposeBag)
    }
    
    
    func setupTopView() {
        
        view.addSubview(topView)
        
        topView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        topView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        topView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        topView.heightAnchor.constraint(equalToConstant: 100).isActive = true
    }
    
    func setupTextFields() {
        saveButton.setTitleColor(.gray, for: .disabled)
        saveButton.setBackgroundColor(.lightGray, for: .disabled)
        
        let stackView = UIStackView(arrangedSubviews: [firstNameTextField, lastNameTextField, phoneNumberTextField, saveButton])
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        //add stack view as subview to main view with AutoLayout
        view.addSubview(stackView)
        stackView.topAnchor.constraint(equalTo: topView.bottomAnchor, constant: 40).isActive = true
        stackView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 40).isActive = true
        stackView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -40).isActive = true
        stackView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
