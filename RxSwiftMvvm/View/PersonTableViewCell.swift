//
//  PersonTableViewCell.swift
//  RxSwiftMvvm
//
//  Created by R+IOS on 15/06/22.
//

import UIKit
import SwiftUI

class PersonTableViewCell: UITableViewCell {
    let cardView: UIView = {
       let view = UIView()
       view.layer.cornerRadius = 14
       view.backgroundColor = .white
       view.translatesAutoresizingMaskIntoConstraints = false
       return view
    }()
    
    let profileImageView: UIImageView = {
       let imageView = UIImageView()
       imageView.layer.cornerRadius = 25
       imageView.clipsToBounds = true
       imageView.contentMode = .scaleAspectFill
       imageView.translatesAutoresizingMaskIntoConstraints = false
       return imageView
    }()
    
    let usernameLabel: UILabel = {
       let label = UILabel()
       label.text = "@itsdanielkioko"
       label.textAlignment = .left
       label.font = UIFont.systemFont(ofSize: 14)
       label.translatesAutoresizingMaskIntoConstraints = false
       return label
    }()
    
    let phoneNumberLabel: UILabel = {
        let label = UILabel()
        label.text = "+628y848243"
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
       super.init(style: style, reuseIdentifier: "cell")
        
        //1st LAYER - UIView
        addSubview(cardView)
        cardView.topAnchor.constraint(equalTo: topAnchor, constant: 12).isActive = true
        cardView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 12).isActive = true
        cardView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12).isActive = true
        cardView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12).isActive = true
        
        setupUI()
        selectionStyle = .none
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI() {
        cardView.addSubview(profileImageView)
        profileImageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        usernameLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        phoneNumberLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        let stackV = UIStackView(arrangedSubviews: [usernameLabel, phoneNumberLabel])
        stackV.axis = .vertical
        stackV.spacing = 10
        stackV.distribution = .fillEqually
        stackV.translatesAutoresizingMaskIntoConstraints = false
                
        let stackH = UIStackView(arrangedSubviews: [profileImageView, stackV])
        stackH.axis = .horizontal
        stackH.spacing = 10
        stackH.distribution = .fill
        stackH.translatesAutoresizingMaskIntoConstraints = false
                 
        cardView.addSubview(stackH)
    }
    
}
