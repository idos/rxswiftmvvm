//
//  PersonViewModel.swift
//  RxSwiftMvvm
//
//  Created by R+IOS on 15/06/22.
//

import Foundation
import RxSwift
import RxCocoa

class PersonViewModel {
    var persons = BehaviorRelay<[Person]>(value: [])
    
    var service: ServiceAPI
    
    init(service: ServiceAPI = ServiceAPI()) {
        self.service = service
    }
    
    func getPerson() {
        service.requestPersons { result in
            switch result {
            case .success(let persons):
                guard let data  = persons else {
                    return
                }
                print("jumlah data saat ini: \(data.count)")
                DispatchQueue.main.async {
                    self.persons.accept(data)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}
