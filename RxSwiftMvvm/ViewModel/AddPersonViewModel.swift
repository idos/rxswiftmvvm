//
//  AddPersonViewModel.swift
//  RxSwiftMvvm
//
//  Created by R+IOS on 15/06/22.
//

import Foundation
import RxSwift
import RxCocoa

class AddPersonViewModel {
    var service: ServiceAPI
    
    var firsNmae = BehaviorRelay<String?>(value: "")
    var lastName = BehaviorRelay<String?>(value: "")
    var phoneNumber = BehaviorRelay<String?>(value: "")
    
    var isValidForm: Observable<Bool> {
        return Observable.combineLatest(firsNmae, lastName, phoneNumber) { firstName, lastName, phone in
            guard firstName != nil && lastName != nil && phone != nil else {
                return false
            }
            
            return !(firstName!.isEmpty) && !(lastName!.isEmpty) && (phone!.count > 10)
        }
    }
    
    init(service: ServiceAPI = ServiceAPI()) {
        self.service = service
    }
    
    func addPerson(completion: @escaping (_ result: Bool) -> Void) {
        let params = ["firstname": firsNmae.value,
                      "lastname": lastName.value,
                      "phonenumber": phoneNumber.value]
        service.addPerson(params: params as [String : Any]) { result in
            switch result {
            case .success(_):
                completion(true)
                
            case .failure(_):
                completion(false)
            }
        }
    }
}
